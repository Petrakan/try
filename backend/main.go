package main

import (
	"github.com/gofiber/fiber/v2"
)

func main() {

	//Инициализируем Fiber
	app := fiber.New()

	//Роуты и хэндлеры
	app.Get("/", controllers.getHomepage)

	//Настройки порта
	err := app.Listen(":3000")
	if err != nil {
		panic(err)
	}
}

//Хэндлер функции
func getHomepage(c *fiber.Ctx) error {
	return c.SendString("Hello Fiber!")
}
