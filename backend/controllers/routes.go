package controllers

import "github.com/gofiber/fiber/v2"

func getHomepage(c *fiber.Ctx) error {
	return c.SendString("Hello fiber! This is homepage")
}
